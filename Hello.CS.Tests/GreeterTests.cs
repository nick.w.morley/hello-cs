using NUnit.Framework;

namespace Hello.CS.Tests
{
    public class GreeterTests
    {
        [Test]
        public void SaysHello()
        {
            Assert.That(Greeter.Say(), Does.Contain("Hello"));
        }
    }
}